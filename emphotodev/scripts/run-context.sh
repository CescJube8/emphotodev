#!/bin/bash -i
source ~/.bashrc
source /emphotodev/venv/bin/activate
# $1 : directory of .tex files to be processed
cd $1
# $2: .tex file to convert
context $2
