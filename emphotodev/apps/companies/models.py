from django.db import models
from image_cropping import ImageCropField, ImageRatioField
from django.utils.html import mark_safe


class Photo(models.Model):
    image = ImageCropField(blank=True, upload_to='photos')
    cropping = ImageRatioField('image', '2560x1550', size_warning=True)
    slide = models.BooleanField(default=False)
    text = models.CharField(max_length=20, blank=True)
    caption = models.CharField(max_length=50, blank=True)

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150px" />' % (self.image))

    image_tag.short_description = 'Image'

    def __str__(self):
        return str(self.pk)


class Category(models.Model):
    cover = models.FileField(upload_to="categories")
    label = models.CharField(max_length=30)
    description = models.TextField(max_length=1000, blank=True, null=True)
    info = models.TextField(max_length=1000, blank=True, null=True)
    photos = models.ManyToManyField(Photo, blank=True)
    cit = models.CharField(max_length=256, blank=True)
    order = models.IntegerField(default=0, blank=True)
    home_page = models.BooleanField(default=False)
    video = models.CharField(max_length=1000, blank=True, null=True)

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150px" />' % (self.cover))

    image_tag.short_description = 'Cover'

    def __str__(self):
        return str(self.label)


class Album(models.Model):
    title = models.CharField(max_length=256, blank=True,help_text="Es. Mario & Gina")
    description = models.TextField(max_length=1000, blank=True, null=True)
    category = models.ForeignKey(Category, blank=True, null=True)
    cover = models.FileField(upload_to="albums",help_text="Immagine di copertina",blank=True)
    photos = models.ManyToManyField(Photo)

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="100px" />' % (self.cover))

    image_tag.short_description = 'Cover'

    def __str__(self):
        return str(self.description)
