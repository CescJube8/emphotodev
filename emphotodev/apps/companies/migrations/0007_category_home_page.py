# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-10-20 19:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('companies', '0006_auto_20161020_1823'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='home_page',
            field=models.BooleanField(default=False),
        ),
    ]
