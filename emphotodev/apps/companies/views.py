from django.views import generic
from .models import Category, Photo, Album
from django.db.models import Q
from .forms import ContactForm
from django.views.generic.edit import FormView
from django.core.mail import send_mail
from django.core.mail import EmailMessage



class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = "category.html"

    def get_object(self, queryset=None):
        return Category.objects.get(label__icontains=self.kwargs['label'])

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        context['menu'] = Category.objects.filter(home_page=False).order_by('order')
        context['slides'] = Photo.objects.filter(category=self.get_object(), slide=True)
        context['albums'] = Album.objects.filter(category=self.get_object())
        return context


class AlbumDetailView(generic.DetailView):
    model = Album
    template_name = "album.html"

    def get_context_data(self, **kwargs):
        context = super(AlbumDetailView, self).get_context_data(**kwargs)
        context['menu'] = Category.objects.filter(home_page=False).order_by('order')
        context['albums'] = Album.objects.filter(category=self.get_object().category).filter(
            ~Q(pk=self.get_object().pk))

        return context


class ContactView(FormView):
    template_name = 'contact.html'
    form_class = ContactForm
    success_url = '/'

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def send_mail(self, form):
        subject = "Richiesta informazioni"
        message = form.cleaned_data["message"]
        sender = form.cleaned_data["email"]
        recipients = ['caspani87@gmail.com']

        message = message + " " + sender

        email = EmailMessage(subject, message, to=recipients)
        email.send()

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        self.send_mail(form)
        return super(ContactView, self).form_valid(form)


class CategoryDetailSliderView(generic.DetailView):
    model = Category
    template_name = "category-slider.html"

    def get_object(self, queryset=None):
        return Category.objects.get(label__icontains=self.kwargs['label'])

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailSliderView, self).get_context_data(**kwargs)
        context['menu'] = Category.objects.filter(home_page=False).order_by('order')
        context['slides'] = Photo.objects.filter(category=self.get_object(), slide=True)
        return context


class HomeDetailView(CategoryDetailView):
    model = Category
    template_name = "home.html"

    def get_object(self, queryset=None):
        return Category.objects.get(label__icontains="home")

    def get_context_data(self, **kwargs):
        context = super(HomeDetailView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.filter(home_page=False).order_by('order')
        return context


class AboutDetailView(CategoryDetailView):
    model = Category
    template_name = "about.html"

    def get_object(self, queryset=None):
        return Category.objects.get(label__icontains="home")

    def get_context_data(self, **kwargs):
        context = super(AboutDetailView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.filter(home_page=False).order_by('order')
        return context
