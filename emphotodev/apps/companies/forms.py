from django import forms


class ContactForm(forms.Form):
    first_name = forms.CharField(max_length=100,required=False)
    last_name = forms.CharField(max_length=100,required=False)
    telephone = forms.CharField(max_length=100,required=False)
    message = forms.CharField(widget=forms.Textarea,required=False)
    email = forms.EmailField(required=False)
