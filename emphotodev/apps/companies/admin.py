from django.contrib import admin
from .models import *
# Register your models here.
from image_cropping import ImageCroppingMixin


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('image_tag', 'label', 'order', 'cit')


class AlbumAdmin(admin.ModelAdmin):
    list_display = ('description','category','image_tag')

class PhotoAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('pk', 'image_tag', 'slide', 'text', 'caption', 'cropping')


admin.site.register(Photo, PhotoAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Category, CategoryAdmin)
