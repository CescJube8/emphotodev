(function ($) {
    $(function () {

        // Initialize collapse button
        $(".button-collapse").sideNav({
            closeOnClick: true
        });
        // Initialize collapsible (uncomment the line below if you use the dropdown variation)
        $('.collapsible').collapsible();


        // init Masonry
        var $grid = $('.grid').masonry({
            itemSelector: '.grid-item',
            percentPosition: true,
            columnWidth: '.grid-sizer'
        });

        // layout Isotope after each image loads
        $grid.imagesLoaded().progress(function () {
            $grid.masonry();
        });

        $('select').material_select();


        // Text hover effect on categories

        var textHover = $(".grid-item");
        $(textHover).each(function () {
            $(this).on('mouseover', function () {

                var item = $(this).find("span");
                var text = $(item).text();
                $(item).data("data-temp", text);
                //   $(item).text("");
                $(item).addClass("categories-text-hover");

            });

            $(this).on('mouseout', function () {
                var item = $(this).find("span");
                var text = $(item).data("data-temp");
                $(item).removeClass("categories-text-hover");

                $(item).text(text);
            });


        });

    }); // end of document ready
})(jQuery); // end of jQuery name space