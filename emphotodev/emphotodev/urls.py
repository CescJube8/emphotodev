"""emphotodev URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url, patterns
from django.conf.urls.static import static
from django.contrib import admin
from apps.companies.views import HomeDetailView, CategoryDetailView, CategoryDetailSliderView, AboutDetailView, \
    AlbumDetailView, ContactView

urlpatterns = patterns('',

                       # other url patterns
                       url(r'^admin', admin.site.urls),
                       url(r'^$', HomeDetailView.as_view(), name="home"),
                       url(r'^about-me/$', AboutDetailView.as_view(), name="about_me"),
                       url(r'^contact/$', ContactView.as_view(), name="contact"),
                       url(r'^categorie/(?P<label>[\w-]+)', CategoryDetailView.as_view(), name="category_detail"),
                       url(r'^album/(?P<pk>[0-9]+)/$', AlbumDetailView.as_view(), name="album_detail"),
                       url(r'^categoriefull/(?P<label>[\w-]+)', CategoryDetailSliderView.as_view(),
                           name="category_detail_full"),
                       # autocomplete urls

                       # comments
                       )

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

### DJANGO REST FRAMEWORK ###

from rest_framework import routers

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = urlpatterns + [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
